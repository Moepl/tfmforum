<?php
require_once 'settings.php';
?>
<meta charset="utf-8">
<title>Transformice Forum Mirror - Deflashed!</title>
<link
    href="css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
<link
    href="css/transformice/general.css" rel="stylesheet" />
<link
    rel="stylesheet" type="text/css" href="markitup/skins/simple/style.css" />
<link
    rel="stylesheet" type="text/css" href="markitup/sets/default/style.css" />
<link
    href="css/jScrollbar.jquery.css" rel="stylesheet" />
<link
    href="lightbox/css/lightbox.css" rel="stylesheet" />

<!--<script language="JavaScript" type="text/javascript" src="js/jquery-1.9.0.js"></script>-->


<script
language="JavaScript" type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script language="JavaScript"
type="text/javascript" src="js/jquery-ui-1.10.0.custom.js"></script>



<?php
$server = 'http://' . $_SERVER['SERVER_NAME'];
if ($runMode != 'debug') {
    $server = $appserver;
}
echo "<script language=\"JavaScript\" type=\"text/javascript\" src=\"$server:8088/socket.io/socket.io.js\"></script>";
?>

<script
src="lightbox/js/lightbox.js"></script>
<script language="JavaScript"
type="text/javascript" src="history.js/scripts/bundled/html4+html5/jquery.history.js"></script>
<script language="JavaScript"
type="text/javascript" src="js/transformice-server.js"></script>
<script language="JavaScript"
type="text/javascript" src="js/transformice-general.js"></script>
<script
type="text/javascript" src="js/jquery-mousewheel.js"></script>
<script
type="text/javascript" src="js/jScrollbar.jquery.js"></script>
<script
language="JavaScript" type="text/javascript" src="js/date.js"></script>
<!--markitUP forumeditor-->
<script
type="text/javascript" src="markitup/jquery.markitup.js"></script>
<script
type="text/javascript" src="markitup/sets/default/set.js"></script>

<script>
    var socket;
    var loggedin=false;
    var maypost=true;

<?php
if ($canpost === false)
    echo 'maypost=false;';
?>
        
    var defaultlang="en";
    var currentthread=0;
    var currentpage=0;
    var currentLangPostId=0;
    var ourloginname;
    var oldLocation = location.href;
    var  baseurl;
    var langs=new Array("FR","EN","BR","RU","TR","CN","ES","NO","PL","HU","NL","RO","ID")



    var History = window.History; // Note: We are using a capital H instead of a lower h
  History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
        var State = History.getState(); // Note: We are using History.getState() instead of event.state
        
      //  History.log(State.data, State.title, State.url);
    });




</script>
