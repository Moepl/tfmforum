// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2011 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
var mySettings = {
    previewParser: function(content) {
    return postTextCleaner(content);
}, 
        previewAutoRefresh:      true,	                         
        previewPosition:         'before',
        resizeHandle:            false,
	onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
	onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
	onTab:    		{keepDefault:false, replaceWith:'    '},
	markupSet:  [ 	
          {name:'Picture', key:'P', replaceWith:'[img][![Url]!][/img]'},
          {name:'Link', key:'L', openWith:'[url=[![Url]!]]', closeWith:'[/url]', placeHolder:'Your text to link here...'},
          {name:'Colors', openWith:'[color=[![Color]!]]', closeWith:'[/color]', dropMenu: [
              {name:'Yellow', openWith:'[color=#FFFF00]', closeWith:'[/color]', className:"col1-1" },
              {name:'Orange', openWith:'[color=#FFA500]', closeWith:'[/color]', className:"col1-2" },
              {name:'Red', openWith:'[color=#FF0000]', closeWith:'[/color]', className:"col1-3" },
              {name:'Blue', openWith:'[color=#0000FF]', closeWith:'[/color]', className:"col2-1" },
              {name:'Purple', openWith:'[color=#800080]', closeWith:'[/color]', className:"col2-2" },
              {name:'Green', openWith:'[color=#00FF00]', closeWith:'[/color]', className:"col2-3" },
              {name:'White', openWith:'[color=#FFFFFF]', closeWith:'[/color]', className:"col3-1" },
              {name:'Gray', openWith:'[color=#808080]', closeWith:'[/color]', className:"col3-2" },
              {name:'Black', openWith:'[color=#000000]', closeWith:'[/color]', className:"col3-3" }
      ]},
          {name:'Quotes', openWith:'[quote]', closeWith:'[/quote]'}, 
          {name:'Clean', className:"clean", replaceWith:function(h) { return h.selection.replace(/\[(.*?)\]/g, "") } },
          {name:'Preview', className:"preview", call:'preview' }
	]
}
