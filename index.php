<!doctype html>
<head>
    <?php
    require_once 'putHeader.php';
    ?>
</head>
<body>
    <div class="maincontainer">

        <table width="100%">
            <tr>
                <td colspan="2" class="contentarea">
                    <table style="border-spacing: 0px;" width="100%">
                        <tr>
                            <td align="left"><img src="img/transformice.png" /></td>
                            <?php
                            if ($cansearch === true) {

                                $defaultarea = '<tr><td><input name="defaultsearch" id="searchareaid"/></td><td><button style="font-size:0.75em;" id="searchbtn">search</button></td>';
                                $searchoptions = '<tr><td><input type="radio" name="searchopt" value="thread" checked>ThreadTitles<input type="radio" name="searchopt" value="post">Contents</td></tr>';

                                $searchdiv = "<td><table>$defaultarea$searchoptions</table></td>";
                                echo $searchdiv;

                                $jquery = '<script>$("#searchbtn").button()</script>';

                                //echo $jquery;
                            }
                            ?>
                        <script>
                            $("#searchbtn").button().click(function(){
                                var searchstring=$('#searchareaid').val();
                                var option=$("input[name='searchopt']:checked").val();

                                $.post("search.php", {
                                    searchstring: searchstring,
                                    searchoption:option
                                },
                                function(data) {
                                    putSearchResults(data);
                                });

								
                            });
                        </script>
                        <?php
                        if ($canlogin === true) {
                            $nicknameinput = "<td></td>\n<td><input name=\"nickname\" id=\"nicknameinput\" value=\"nickname\"></td>\n";
                            $passwordinput = "<td></td>\n<td><input name=\"password\" type=\"password\" id=\"passwordinput\" value=\"***\"/></td>\n";

                            $button1 = "<button class=\"navbutton\" id=\"loginbutton\">login</button>";
                            echo "<td  id=\"loginarea\" rowspan=\"2\" align=\"center\"><table>\n<tr>$nicknameinput</tr>\n<tr>$passwordinput<td>$button1</td>\n</tr>\n</table>\n</td>";

                            echo "<script>\n";

                            echo "$(\"#loginbutton\").button().click(function() {var nick=$(\"#nicknameinput\").val();\nvar pw=$(\"#passwordinput\").val();$(\"#loginbutton\").button(\"disable\");login(nick, pw);});\n";
                            echo "</script>\n";
                        }
                        ?>

            </tr>
            <tr>
                <td align="left"><span style="font-size: 0.7em;" id="onlinemice"></span>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>

    <td class="contentarea" width=25%>
        <div id="threadcontainer">
            <div id="ForumsAndThreads" class="scrollcontainer"></div>
            <?php
            if ($canpost)
                echo '<div class="contentarea" id="newtopicdiv"></div>';
            ?>
        </div>
    </td>
    <td valign="top">
        <div id="Thread-Title-On-Content"></div>
        <div id="posterandthreadcontent" class="scrollcontainer">
            <div id="Threadcontent"></div>
            <?php
            if ($canpost)
                echo '<div class="contentarea" id="markitupdiv"></div>';
            ?>
        </div>
    </td>
</tr>
</table>

</div>


<script>
    $( document ).ready( function() {
        baseurl=window.location.pathname;
            
        var modi=0.75;
            
        var height= $(window).height();
        console.log('height: '+height);
        $("#ForumsAndThreads").css('height', height*modi);
        $("#posterandthreadcontent").css('height', height*modi);
            
        $(window).resize(function() {
            var height= $(window).height();
            $("#ForumsAndThreads").css('height', height*modi);
            $("#posterandthreadcontent").css('height', height*modi);
        });
        //  $('.scrollcontainer').scrollbars();	
    
<?php
$server = 'http://' . $_SERVER['SERVER_NAME'];
echo "socket = io.connect('$server:8088');";
?>
    
        //            socket = io.connect('http://moepl.eu:8088');
        
        loadDataOnUrl(true);
        //act on user pressing the forward/backward button on his borwser
        setInterval(function() {
            if(location.href != oldLocation) {
                //    do your action
                console.log("something changed");
                loadDataOnUrl(false);
                oldLocation = location.href
            }
        }, 500); 
        socket.on('loginresult',function(data){
               
            var login=JSON.parse(data).login;
            var result=login.result;
            console.log(result);
            if (login.result)
            {
                console.log("clearing");
                $('#loginarea').empty();
                var div=login.nomJoureur;
                $('#loginarea').append(div);
                ourloginname=login.nomJoureur;
                loggedin=true;
                requestForumList();
                requestThread(currentthread, currentpage);
             
            }
            else{
                $('#loginbutton').button("enable");
            }
            console.log(login);
               
        });
            
        socket.on('onlinemice',function(data){
            var onlinemice=JSON.parse(data);
            var mice=onlinemice.online;
            $('#onlinemice').empty();
            $('#onlinemice').append("online: "+mice);  
        });
       
        socket.on('forumlist',function(data)
        {
            putForumList(data);
      
        });   

        socket.on('threadlist',function(data)
        {
            putThreadList(data);
        });
        
               
        socket.on('thread',function(data)
        {
                           

            putThread(data);
        });
        
        socket.on('newmessage',function(data)
        {
            var newmessage=JSON.parse(data);
            var threadcode=newmessage.threadcode;
            var date=newmessage.date;
            var name=newmessage.name;
            
            if (threadcode==currentthread)
            {
                requestThread(currentthread,currentpage);
            }
            console.log(newmessage); 
        });
  
    });        


</script>

</body>
</html>
