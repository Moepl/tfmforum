var net = require('net');


var io = require('socket.io').listen(Number(process.env.VCAP_APP_PORT) ||8088, {
    log : false
});

io.configure('production', function(){
  io.enable('browser client etag');
  io.set('log level', 1);

  io.set('transports', [
 'htmlfile'
  , 'xhr-polling'
  , 'jsonp-polling'
  ]);
});
io.sockets.on('connection', function(socket) {
    var tfmforum = require('./api/TFMForumConnection');
    var tfmclient = new net.Socket();

    function sendDataToUser(event, data) {
        socket.emit(event, data);
    }

    tfmforum.connect(tfmclient, sendDataToUser);

    socket.emit('news', {
        hello : 'world'
    });
    socket.on('requestforum', function(data) {

        if (data.forum)
            tfmforum.requestForum(tfmclient, data.forum, data.page);

    });
    socket.on('forumlist', function(data) {
        tfmforum.requestForumList(tfmclient);
    });
    socket.on('requestthread', function(data) {
        tfmforum.requestThread(tfmclient, data.thread, data.page);
    });
    socket.on('login', function(data) {
        tfmforum.requestLogin(tfmclient, data.nickname, data.password);
    });
    socket.on('post', function(data) {
        console.log("got post");
        tfmforum.requestPostSend(tfmclient, data.post, data.text);

    });
    socket.on('editpost', function(data) {
        console.log("got edit post");
        tfmforum.requestPostEdit(tfmclient, data.thread, data.post, data.text);
    });
    socket.on('newthread',function(data){
        console.log(data);
        tfmforum.requestNewThreadSend(tfmclient,data.forum,data.threadtitle,data.content);
    });
});
