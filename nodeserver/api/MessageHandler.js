module.exports = {
    parser: function (data, socket, sendPacket) {
        var offset = 0;
        var len = readInt();
        var c = readbyte();
        var cc = readbyte();
        console.log('LEN: ' + len+ ' data.length: '+data.length);
	
	
	
        console.log(c + ' ' + cc);

        //   console.log('DATA: ' + data.toString('hex'));
        if (c == 2) {

            if (cc == 2) {
                return    readForumList();
            }
            else if (cc==4)
            {
                return readThreadList();
            }
            else if (cc==5)
            {
                return readThreadResponse();
            }
            else if (cc==10)
                {
                    return readNewMessage();
                }
        } else if (c == 40) {
            if (cc==2)
            {
                return     readLogin();
            }
            else   if (cc == 40) {
                console.log('have to send a ping');
                
                var onlinemice=readInt();
                console.log('online: '+onlinemice);
                
                var buf = new Buffer(2);
                buf.writeInt8(40, 0);
                buf.writeInt8(40, 1);
                sendPacket(socket, buf);
                return ['onlinemice',JSON.stringify({
                    online:onlinemice
                })];
            }

        }

        function readbyte() {
            var value = data.readInt8(offset);
            offset++;
            return value;
        }

        function readshort() {
            var value = data.readInt16BE(offset);
            offset += 2;
            return value;
        }

        function readInt() {
            var value = data.readInt32BE(offset);
            offset += 4;
            return value;
        }

        function readUTF8() {
            var length = readshort();
            var text = data.toString('utf8', offset, offset + length);
            offset += length;
            return text;
        }

        function readNewMessage()
        {
            var threadcode=readInt();
            var date=readInt();
            var name=readUTF8();
    
            var message=new Object();
            message.threadcode=threadcode;
            message.date=date;
            message.name=name;
    
            return ['newmessage',JSON.stringify(message)];
    
        }

        function readLogin(){
            var loginobject=new Object();
            var loginworked=readbyte()==1;
            loginobject.result=loginworked;
            if (loginworked)
            {
                var nomJoueur=readUTF8();
                var avatar=readInt();
                var typeauteur=readInt();
                loginobject.nomJoureur=nomJoueur;
                loginobject.avatar=avatar;
                loginobject.typeauteur=typeauteur;
            }
            console.log(loginobject);
            return ['loginresult',JSON.stringify({
                login:loginobject
            })];
    
        }
        function readThreadResponse()
        {
            var threadcode=readInt();
            //no clue
            var edition=readbyte();
            //I guess some sent stuff
            var peutSuppr=readbyte();
            // if thread is locked flag
            var peutRepondire=readbyte();
            //no clue
            var fin=readbyte();
            //no clue
            var modtribu=readbyte();
            //threadtitle ofc
            var titre=readUTF8();
            //current page of thre thread (20 posts per page)
            var pageEnCours=readshort();
            //max pages of the thread
            var nombrePage=readshort();
            //no clue
            var message=readbyte();
            var thread=new Object();
    
            var posts=new Array();
    
            thread.threadcode=threadcode;
            thread.edition=edition;
            thread.peutsuppr=peutSuppr;
            thread.peutrepondre=peutRepondire;
            thread.fin=fin;
            thread.modtribu=modtribu;
            thread.title=titre;
            thread.pageencours=pageEnCours;
            thread.nombrepage=nombrePage;
            thread.message=message;
    
            thread.posts=posts;
    
            console.log(thread);
    
            while (offset<len)
            {
                var post=new Array();
                
                var postid=readInt();
                //the lower the id, the more rights the user has
                var typeauteurmessage=readbyte();
                //username
                var auteur=readUTF8();
                //no clue
                var typeauteurcourant=readbyte();
                //avatarid
                var avatar=readInt();
                //unixtime
                var date=readInt();
                //postconent
                var texte=cleanUpPost(readUTF8());
                //no clue
                var infos=readUTF8();
                //no clue
                var type=readbyte();
                // I don't think that is even used on the original client...'
                var multilang=readbyte();
                //         

                post.push(postid,typeauteurmessage,auteur,typeauteurcourant,avatar,date,texte,infos,type,multilang);
                posts.push(post);
    
            }
            return ['thread',JSON.stringify(thread)];
        }
        function readThreadList()
	
        {
            var cancreate=readbyte();
            console.log('can create: '+cancreate);
	  
            var forumcode=readInt();
            //language
            var payscountry=readUTF8();
            //icon
            var icone=readshort();
            // boolean - is tribeforum or not
            var tribu=forumcode>10000;
            var tribename=(tribu?readUTF8():"");
            var indexpage=readInt();
            var maxpage=readInt();
            //no clue
            var masquersujetcourant=readbyte();
            var thisforum=new Object();
            var forumname=tribu?tribename:getForumName(forumcode,payscountry);

            thisforum.forumcode=forumcode;
            thisforum.payscountry=payscountry;
            thisforum.forumname=forumname;
            thisforum.icone=icone;
            thisforum.tribu=tribu;
            thisforum.indexpage=indexpage;
            thisforum.maxpage=maxpage;
            thisforum.masquersujetcourant=masquersujetcourant;
	  
            // console.log(thisforum);

            var threads=new Array();
	  
            while (offset<len)
            {
                //   console.log(offset+' '+data.length);
                var threadcode=readInt();
                // console.log(threadcode);
                var threadtitle=readUTF8();
                //  console.log(threadtitle);
                var date=readInt();
                var threadauthor=readUTF8();
                var typeauteur=readbyte();
                var lastposter=readUTF8();
                var numberofmessages=readshort();
                var lockbyte=readbyte();
                var stickied=readbyte()>0;
                
                var thread=new Array(threadcode,threadtitle,date,threadauthor,typeauteur,lastposter,numberofmessages,lockbyte,stickied);
                console.log(thread);
                threads.push(thread);
            }
            thisforum.threads=threads;
	  
            // console.log(thisforum);
	  
            return ['threadlist',JSON.stringify(thisforum)];
	  
        }
        
        function readForumList()

        {

            var defaultlang = readUTF8();
            //   console.log(defaultlang);
            var forumlist = new Object();
            forumlist.defaultlang=defaultlang;
            var forums = new Array();
            var alllangs=new Array();
            while (offset < len) {
                var code = readInt();
                var lang = readUTF8();
                var icon = readshort();
                var tribeforum = code > 10000;
                var forumname;
                if (tribeforum)
                {
                    forumname=readUTF8();
                }
                else {
                    forumname=getForumName(code,lang); 
                }
                var forum = new Array(code, lang,forumname, icon, tribeforum);
                forums.push(forum);
		
                if (alllangs.indexOf(lang)==-1)
                {
                    alllangs.push(lang);
                }
		  
            }               
	    
            var langsforum=new Array();
            for (var k=0;k<alllangs.length;k++){
                var newlang=new Array();
                newlang.push(alllangs[k]);
                langsforum.push(newlang);
            }
            console.log(langsforum);
            for (var i=0;i<forums.length;i++)
            {
                var index=alllangs.indexOf(forums[i][1]);
                langsforum[index].push(forums[i]);
            }
            forumlist.forums=langsforum;

            //console.log(langsforum);
            return ['forumlist',JSON.stringify(forumlist)];


        }



    }
};


function cleanUpPost(post)
{
    
    var cleaned=post;
    
    cleaned = cleaned.replace(/\n/g,"<br />");
    
    
    return cleaned;
    
}

function getForumName(code,lang)
{
    if (code==1)
        return "Announcements";
    else if (code==400)
        return "Atelier 801";
    else if (code==401)
        return "Sanctions";
    else if (code==402)
        return "Moderators";
    else if (code==403)
        return "Sents";
    else if (code==404)
        return "MapCrew";
    else if (code==405)
        return "Tigrounette";
    else if (code==406)
        return "Arbs";
    else if (code==407)
        return "MapSubmissions";
    else if (code==44)
        return "Poubelle";
    else{
        //regular forum
        var regforum=code%20;
    
        if (regforum==1)
            return "Discussions";
        else if (regforum==2)
            return "Off topic";
        else if (regforum==3)
            return "MapEditor";
        else if (regforum==4)
            return "Tribes";
        else if (regforum==5)
            return "Fanart";
        else if (regforum==6)
            return "Suggestions";
        else if (regforum==7)
            return "Bugs";
        else if (regforum==8)
            return "ForumGames";
        else if (regforum==9)
            return "Signalisations";
        else if (regforum==10)
            return "MapSubmissions";
        else if (regforum==11)
            return "Archives";
    
    
    
    }
  
  
  
  
    return "trololo";
}
