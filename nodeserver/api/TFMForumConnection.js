var HOST = '37.59.46.220';
var PORT = 443;
var version = 17;
var messagehandler = require('./MessageHandler');
module.exports = {
    connect: function (tfmclient,sendfunction) {
        var buffereddata = new Buffer(0);
        tfmclient.connect(PORT, HOST, function () {
            console.log('CONNECTED TO: ' + HOST + ':' + PORT);
            tfmclient.connected=true;
            sendPacket(tfmclient, sendVersion(version));
        });


        // Add a 'data' event handler for the client socket
        // data is what the server sent to this socket
        tfmclient.on('data', function (data) {         
            /*
             * We get the data as a byte array. Now we have 4 cases:
             * 1. the data is a whole transformice packet
             * 2. the data is just the beginning of a transformice packet
             * 3. the data is just the ending of a transformice packet
             * 4. one of the 3 cases with a another transformice packet (whole
             *  or only a part of it) at the beginning or at the end.
             *  
             *  The current solution is saving all data into a buffer and get 
             *  a transformice packet out of the buffer when all data arrived.
             *  that packet is then stripped from the buffer.
             */
            var tempbuffer=new Buffer(buffereddata.length+data.length);
            buffereddata.copy(tempbuffer,0);
            data.copy(tempbuffer,buffereddata.length);
            buffereddata=tempbuffer;
                
            if (buffereddata.length<4)
                return;
                
            var len=buffereddata.readInt32BE(0);
                
            if (len>buffereddata.length)
            {
                return;
            }

            console.log('bufferddata length: '+buffereddata.length+' len: '+len);

            var newdata = buffereddata.slice(0,len);
            buffereddata=buffereddata.slice(len);

            var message = messagehandler.parser(newdata, tfmclient, sendPacket);


            if (message != null) {
                console.log('data is not null');
                if (message[1]) {
                    console.log('sending to client');
                    sendfunction(message[0], message[1]);
                }
            } 
        });

        // Add a 'close' event handler for the client socket
        tfmclient.on('close', function () {
            tfmclient.connected=false;
            console.log('Connection closed');
        });



    // whatever
    },
    requestForum: function (tfmclient,forumcode, pagep) {
        var page = 0;
        if (pagep) page = pagep;
        if (forumcode)
            sendPacket(tfmclient, requestForumPage(forumcode, page));
        console.log('requesting: ' + forumcode + ' ' + page);
    },
    requestForumList: function(tfmclient){
       
        sendPacket(tfmclient,requestForumListDemande());
       
    },
    requestThread:function(tfmclient,threadcode,pagep){
        if (threadcode)
        {
            var code=threadcode;
            var page=pagep;
            if (!pagep)
            {
                page=0;
            }
            sendPacket(tfmclient,requestThreadPage(code,page));
        }
            
    },
    requestLogin:function(tfmclient,nickname,password)
    {
        if (nickname&&password&&isString(nickname)&&isString(password))
        {
            sendPacket(tfmclient,login(nickname,password));
        }
    },
    requestPostSend:function(tfmclient,threadcode,text){
        if (threadcode&&text&&isNumber(threadcode)&&isString(text)){
            console.log("can post to tfm server now");
            sendPacket(tfmclient, sendPost(threadcode,text));
        }
    },
    requestPostEdit:function(tfmclient,threadcode,post,text){
        sendPacket(tfmclient, sendEdit(threadcode,post,text));
    },
    requestNewThreadSend:function(tfmclient,forumcode,threadtitle,content){
        if (forumcode&&threadtitle&&content&&isNumber(forumcode)&&isString(threadtitle)&&isString(content))
            sendPacket(tfmclient, sendNewThread(forumcode,threadtitle,content));
    }
};

function requestForumPage(forumcode, page){
    var buf = new Buffer(10);
    buf.writeInt8(2, 0);
    buf.writeInt8(4, 1);
    buf.writeInt32BE(forumcode, 2);
    buf.writeInt32BE(page, 6);
    return buf;
}
function requestThreadPage(threadcodep,pagep)
{
    var threadcode=threadcodep;
    var page=pagep;
    if (!isInt(threadcode)||!isShort(page))
    {
        console.log(typeof(threadcode));
        threadcode=0;
        page=0;
    }
    threadcode=parseInt(threadcode);
    page=parseInt(page);
         
    //
    var unknownthingy=false;
    var buf = new Buffer(11);
    buf.writeInt8(2, 0);
    buf.writeInt8(5, 1);
    buf.writeInt32BE(threadcode, 2);
    buf.writeInt16BE(page, 6);
    if (unknownthingy)
        buf.writeInt8(1,10);
    else
        buf.writeInt8(0,10);
    return buf;
    
    
}
function requestForumListDemande()
{
    var buf = new Buffer(2);
    buf.writeInt8(2, 0);
    buf.writeInt8(3, 1);
    return buf;
}
function login(nickname,password){
    var offset=0;
    var buf = new Buffer(7+nickname.length+password.length);
    buf.writeInt8(40, offset);
    offset++;
    buf.writeInt8(2, offset);
    offset++;
    buf.writeInt16BE(nickname.length,offset);
    offset+=2;
    buf.write(nickname,offset);
    offset+=nickname.length;
    buf.writeInt16BE(password.length,offset);
    offset+=2;
    buf.write(password,offset);
    offset+=password.length;
    buf.writeInt8(0, offset);
    return buf;
    
}
function sendPost(thread,post)
{
    var offset=0;
    var buf = new Buffer(8+post.length);
    buf.writeInt8(4, offset);
    offset++;
    buf.writeInt8(4, offset);
    offset++;
    buf.writeInt32BE(thread,offset);
    offset+=4;
    buf.writeInt16BE(post.length,offset);
    offset+=2;
    buf.write(post,offset);
    return buf;
}
function sendEdit(thread,postid,text)
{
    var offset=0;
    var buf = new Buffer(12+text.length);
    buf.writeInt8(4, offset);
    offset++;
    buf.writeInt8(6, offset);
    offset++;
    buf.writeInt32BE(thread,offset);
    offset+=4;
    buf.writeInt32BE(postid,offset);
    offset+=4;
    buf.writeInt16BE(text.length,offset);
    offset+=2;
    buf.write(text,offset);
    return buf;
}
function sendNewThread(forum,threadtitle,content)
{
    var offset=0;
    var buf = new Buffer(10+threadtitle.length+content.length);
    buf.writeInt8(4, offset);
    offset++;
    buf.writeInt8(2, offset);
    offset++;
    buf.writeInt32BE(forum,offset);
    offset+=4;
    buf.writeInt16BE(threadtitle.length,offset);
    offset+=2;
    buf.write(threadtitle,offset);
    offset+=threadtitle.length;
    buf.writeInt16BE(content.length,offset);
    offset+=2;
    buf.write(content,offset);
    return buf;
    
    

}
function sendVersion(version) {
    var buf = new Buffer(6);
    buf.writeInt8(28, 0);
    buf.writeInt8(1, 1);
    buf.writeInt32BE(version, 2);
    return buf;
}
function sendPacket(socket, data) {
    /*
     * writing the len to each packet.
     */
    var sendbuf = new Buffer(data.length + 4);
    data.copy(sendbuf, 4);
    sendbuf.writeInt32BE(data.length + 4, 0);

    if (socket.connected)
        socket.write(sendbuf);

}

function isInt(num)
{
    if (isNumber(num))
    {
        return (num<214748364&&num>-2147483648);
    }
    else{
        return false;
    }
}
function isShort(num)
{
    if (isNumber(num))
    {
        return (num<32767&&num>-32768);
    }
    else{
        return false;
    }
}
function isNumber(num) {
    return (typeof num == 'string' || typeof num == 'number') && !isNaN(num - 0) && num !== '';
}
function isString(thing){
    return (typeof thing == 'string' || thing instanceof String);
}