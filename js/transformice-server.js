/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function requestForum(forumcode,page)
{
    socket.emit('requestforum', {
        forum: forumcode,
        page:page
    });
}
function requestForumList(lang)
{
    console.log('request forumlist: '+lang);
    socket.emit('forumlist', {
        langname:lang
    });
}
function requestThread(threadcode,pagep)
{
    console.log("got request: "+threadcode+" "+pagep);
    var page=pagep;
    socket.emit('requestthread',{
        thread:threadcode,
        page:page
    });
}
function login(nickname,password)
{
    
    $.post("passwordhasher.php", {
        password: password
    },
    function(data) {
        console.log("login request sending: "+nickname+" "+data);
        socket.emit('login',{
            nickname:nickname,
            password:data
        });
    });

}
function requestForumPostSend(threadcode,text){
    console.log("emitting post!!");
    socket.emit('post',{
        post:threadcode,
        text:text
    });
}
function requestForumPostEdit(threadid,postid,message)
{
    console.log("I want to edit hte post: "+postid+" in thread: "+threadid+" to: "+message);
         socket.emit('editpost',{
            thread:threadid,
            post:postid,
            text:message
        });
}
function requestNewForumThread(forum,threadtitle,message)
{
    console.log("I want to create hte thread: "+threadtitle+" in forum: "+forum+" to: "+message);
         socket.emit('newthread',{
            forum:forum,
            threadtitle:threadtitle,
            content:message
        });
}
