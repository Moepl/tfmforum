/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function putForumList(data) {
    $('#ForumsAndThreads').empty();
    $('#newtopicdiv').empty();

    var forumlist = JSON.parse(data);

    defaultlang = (forumlist.defaultlang);

    var forums = forumlist.forums;

    for ( var i = 0; i < forums.length; i++) {
        if (forums[i][0] == 'xx' || forums[i][0] == defaultlang) {
            var forumlang = forums[i];

            for ( var k = 1; k < forumlang.length; k++) {

                putForumItem(forumlang[k]);
            }
        }
    }
    console.log(forumlist);


}

function putThreadList(data) {
    currentLangPostId = 0;
    $('#ForumsAndThreads').empty();
    $('#newtopicdiv').empty();
    var threadlist = JSON.parse(data);
    var pageencours = threadlist.indexpage;
    var nombrepage = threadlist.maxpage - 1;
    var forumcode = threadlist.forumcode;
    var backallbutton = '<button class="navbutton" id="ballback-' + forumcode
    + '"><<</button>';
    var backbutton = '<button class="navbutton" id="bback-' + forumcode
    + '"><</button>';
    var forwardbutton = '<button class="navbutton" id="bfoward-' + forumcode
    + '">></button>';
    var forwardallbutton = '<button class="navbutton" id="ballforward-'
    + forumcode + '">>></button>';
    var pagenav = "<div>" + (backallbutton + backbutton)
    + '<span style="color:#606090">' + (pageencours + 1) + '/'
    + (nombrepage + 1) + '</span>' + (forwardbutton + forwardallbutton)
    + "</div>";

    $('#ForumsAndThreads').append(pagenav);

    $("#ballback-" + forumcode).button().click(function() {
        requestForum(forumcode, 0);
    });
    $("#bfoward-" + forumcode).button().click(function() {
        requestForum(forumcode, pageencours + 1);
    });

    $("#bback-" + forumcode).button().click(function() {
        requestForum(forumcode, pageencours - 1);
    });
    $("#ballforward-" + forumcode).button().click(function() {
        requestForum(forumcode, nombrepage);
    });

    if (pageencours == 0) {
        $("#ballback-" + forumcode).button("disable");
        $("#bback-" + forumcode).button("disable");
    }
    if (pageencours == nombrepage) {
        $("#bfoward-" + forumcode).button("disable");
        $("#ballforward-" + forumcode).button("disable");
    }

    console.log(threadlist);

    var image = '<img src="' + getImageForum(threadlist.icone) + '">';


    var threadnameflag = '<table><tr><td>' + threadlist.forumname
    + '</td></tr><tr><td align="left"><img src="'
    + getImageLang(threadlist.payscountry) + '"></td></tr></table>';
    var wholethingy = '<div style="cursor: pointer;" onclick="requestForumList(getLang());replaceHistoryToRoot();'
    + '"><table><tr><td align="left">'
    + image
    + '</td><td align="left">'
    + threadnameflag
    + '</td></tr></table></div>';
    var div = '<div class="ui-content-selected">' + wholethingy + '</div>';


    $('#ForumsAndThreads').append(div);

    var threads = threadlist.threads;

    for ( var i = 0; i < threads.length; i++) {
        var thread = threads[i];

        var code = thread[0];
        var title = thread[1];
        var date = thread[2];
        var threadauthor = thread[3];
        var typeauteur = thread[4];
        var lastposter = thread[5];
        var numberofmessages = thread[6];
        var lockbyte = thread[7];
        // console.log("type: "+lockbyte);
        var stickied = thread[8];
        // console.log('lastposter: '+lastposter);
        var top = '<table><tr><td align="left">'
        + (lockbyte == 2 ? '<img src="img/locked.png"  width="10px" height="10px"/>'
            : "") + title
        + '</td><td><span style="font-weight:bold;">'
        + getColorSpan(threadauthor, typeauteur)
        + '</span></td></tr></table>';
        var timeformat = dateFormat(date * 1000, "dd.mm.yy hh:MM:ss");
        var bottom = '<table><tr><td><span style="color: grey;font-size: 8pt">'
        + numberofmessages + '</span></td><td>'
        + getColorSpanShort(lastposter)
        + '</td><td><span style="color: #606090;">' + timeformat
        + '</span></td></tr></table>';

        var onclick = 'requestThread(' + code + ",(" + 'isAlreadyRead(' + code
        + ')' + '?(' + (numberofmessages / 20) + '):' + 0
        + '));selectThread(' + code + ');';
        var whole = '<div style="cursor: pointer;" onclick="' + onclick
        + '"><table><tr><td>' + top + '</td></tr><tr><td>' + bottom
        + '</td></tr></table></div>';
        div = '<div class="'
        + (stickied == true ? "ui-content-sticky" : "ui-content")
        + ' selectable-thread" id="thread-' + code + '">' + whole
        + '</div>';

        $('#ForumsAndThreads').append(div);

    }
    // normal users can't post in forums like announcements or archives
    if (forumcode >= 20 && loggedin) {
        console.log("can make a thread");

        var newthreadbutton = '<button id="newtopicbutton">Create new Thread</button>';
        $('#newtopicdiv').append(newthreadbutton);

        (function() {
            var forum = forumcode;
            $("#newtopicbutton")
            .button()
            .click(
                function() {
                    $('#Threadcontent').empty();
                    $('#Thread-Title-On-Content').empty();
                    var newtopicname = '<textarea id="topicnametextarea">topicname</textarea>';
                    $('#Thread-Title-On-Content').append(
                        newtopicname);

                    var divnewtopiccontent = '<textarea id="newtopicmarkitup"></textarea>';
                    var submittopic = '<div id="sendtopicbuttondiv"><button id="sendtopicbtn">Submit Thread</button></div>';
                    $('#Threadcontent').append(
                        divnewtopiccontent + submittopic);
                    $('#newtopicmarkitup').markItUp(mySettings);
                    $('#sendtopicbtn')
                    .button()
                    .click(
                        function() {
                            var tname = $(
                                '#topicnametextarea')
                            .val();
                            var content = $(
                                '#newtopicmarkitup')
                            .val();

                            requestNewForumThread(
                                forum, tname,
                                content);

                        });
                });
        })();

    }

}
function replaceHistoryToRoot() {
    // console.log("replace state");
    history.replaceState(null, null, baseurl);
}

function isAlreadyRead(threadcode) {

    var check = $("#thread-" + threadcode).hasClass("ui-content-thread-read");
    // console.log('selected: '+check);
    return check;

}

function selectThread(threadcode) {

    // alert('selected: '+threadcode);
    $('.selectable-thread.ui-content-selected').removeClass(
        'ui-content-selected').addClass("ui-content");
    $('.selectable-thread.ui-content.ui-content-sticky').removeClass(
        'ui-content');
    $('#thread-' + threadcode).addClass('ui-content-thread-read');

    $('#thread-' + threadcode).removeClass('ui-content').addClass(
        'ui-content-selected');
}

function putSearchResults(data) {
    var search = JSON.parse(data);

    console.log(search);

    $("#posterandthreadcontent").scrollTop(0);
    $('#Threadcontent').empty();
    $('#markitupdiv').empty();
    $('#Thread-Title-On-Content').empty();
    var searchtitle = '<div class="transformice-thread-header"><table width="100%"><tr><td align="left">'
    + getColorSpan("Search results", 19) + '</td></tr></table></div>';
    $('#Thread-Title-On-Content').append(searchtitle)

    for ( var i = 0; i < search.length; i++) {
        var result = search[i];
        console.log(result);
        var searchlayout = "";
        if (result.post === null||result.post=="") {
            searchlayout = '<div class="transformice-thread-post"><table><tr><td><a href="'+baseurl+'?s='+result.threadid+'&p=1">'	+ result.threadtitle + '</a></td></tr></table></div>';
        }
        else{
            searchlayout = '<div class="transformice-thread-post"><table><tr><td><a href="'+baseurl+'?s='+result.threadid+'&p='+(parseInt(result.onpage)+1)+'">'	+ result.threadtitle + '</a></td></tr><tr><td align="left">'+postTextCleaner("<N>"+result.post)+'</td></tr></table></div>';
        }
        $('#Threadcontent').append(searchlayout);
    }

}

function putThread(data) {
    var thread = JSON.parse(data);
    console.log(thread);

    var threadcode = thread.threadcode;
    var edition = thread.edition;
    var peutSuppr = thread.peutsuppr;
    var peutrepondre = thread.peutrepondre;
    var fin = thread.fin;
    var modtribu = thread.modtribu;
    var title = thread.title;
    var pageencours = thread.pageencours;
    var nombrepage = thread.nombrepage;
    var message = thread.message;
    if (currentthread != threadcode || currentpage != pageencours) {
        $("#posterandthreadcontent").scrollTop(0);
        var haveToEmptyPoster = true;
    }
    if (haveToEmptyPoster) {
        if (currentthread == threadcode) {
            var editortext = $('textarea#markItUp').val();
            if (editortext != "") {
                haveToEmptyPoster = false;
            }
            else{
                console.log("editortext: "+editortext);
            }
        }
    }

    if (peutrepondre == 0) {
        haveToEmptyPoster = true;
    }

    $('#Threadcontent').empty();
    if (haveToEmptyPoster){
        $('#markitupdiv').empty();
    }
    $('#Thread-Title-On-Content').empty();
    if (pageencours == nombrepage && peutrepondre == 1&&(haveToEmptyPoster||(currentpage!=pageencours)) &&maypost) {
        //  console.log("adding textarea... " +$('#markitupdiv').length);
        var editordiv = '<textarea id="markItUp"></textarea><button id="sendreplybutton" onclick="postText('
        + threadcode + ');">Send my reply</button>';
        $('#markitupdiv').append(editordiv);

        $("#markItUp").markItUp(mySettings);
        $("#sendreplybutton").button();

    }
    if (!(currentpage&&threadcode&&pageencours<currentpage))
    History.pushState(null,""+title, '?s='+threadcode+'&p='+(pageencours+1));

    currentthread = threadcode;
    currentpage = pageencours;



    oldLocation = location.href;
    

    var posts = thread.posts;

    var backallbutton = '<button class="navbutton" id="buttonallback-'
    + threadcode + '"><<</button>';
    var backbutton = '<button class="navbutton" id="buttonback-' + threadcode
    + '"><</button>';
    var forwardbutton = '<button class="navbutton" id="buttonfoward-'
    + threadcode + '">></button>';
    var forwardallbutton = '<button class="navbutton" id="buttonallforward-'
    + threadcode + '">>></button>';

    var pagenav = (backallbutton + backbutton) + '<span style="color:#606090">'
    + (pageencours + 1) + '/' + (nombrepage + 1) + '</span>'
    + (forwardbutton + forwardallbutton);


    var div = '<div class="transformice-thread-header"><table width="100%"><tr><td align="left">'
    + getColorSpan(title, 19)
    + '</td><td align="right">'
    + pagenav
    + '</td></tr></table></div>';

    $('#Thread-Title-On-Content').append(div);

    $("#buttonallback-" + threadcode).button().click(function() {
        requestThread(threadcode, 0);
    });
    $("#buttonfoward-" + threadcode).button().click(function() {
        requestThread(threadcode, (pageencours + 1));
    });

    $("#buttonback-" + threadcode).button().click(function() {
        requestThread(threadcode, (pageencours - 1));
    });
    $("#buttonallforward-" + threadcode).button().click(function() {
        requestThread(threadcode, nombrepage);
    });

    if (pageencours == 0) {
        $("#buttonallback-" + threadcode).button("disable");
        $("#buttonback-" + threadcode).button("disable");
    }
    if (pageencours == nombrepage) {
        $("#buttonfoward-" + threadcode).button("disable");
        $("#buttonallforward-" + threadcode).button("disable");
    }

    for ( var i = 0; i < posts.length; i++) {

        var post = posts[i];
        var avatarrow = "";
        var avatar = post[4];
        var date = post[5];
        if (avatar) {
            // console.log(avatar);
            avatarrow = '<tr><td><img src="http://www.transformice.com/avatar/'
            + (avatar % 10000) + '/' + avatar + '.jpg"></td></tr>';
        }
        var timeformatrough = dateFormat(date * 1000, "dd.mm.yyyy");
        var timeformathours = dateFormat(date * 1000, "HH:MM:ss");
        var daterow = '<tr><td class="dateunderauthor">' + timeformatrough
        + ' ' + timeformathours + '</td></tr>';

        var author = '<div class="authorcolumn"><table><tr><td align="left">'
        + getColorSpan(post[2], post[3]) + '</td></tr>' + avatarrow
        + daterow + '</table></div>';
        var posttext = postTextCleaner("<N>" + post[6]);
        var editpost = "yoyo";
        var id = post[0];
        var rawpost = post[6].replace(/<br \/>/g, "\n");

        editpost = '<img class="imagesInPost" id="postedit-'
        + id
        + '" src="img/Tribes.png" height="15" width="15" style="display:none;"/>';

        var postlayout = '<div class="transformice-thread-post"><table border="0"><tr><td valign="top">'
        + author
        + '</td><td align="left" valign="top" id="posttext-'
        + post[0]
        + '">'
        + posttext
        + '</td></tr><tr><td align="left">'
        + editpost + '</td></table></div>';

        $('#Threadcontent').append(postlayout);
        if (loggedin && peutrepondre && ourloginname == post[2] && maypost) {
            $("#postedit-" + id).show();
            var editdialogdiv = '<div id="editdialogpost-'
            + id
            + '" class="" title="Edit Post"><textarea id=postedittextara-'
            + id + '>' + rawpost
            + '</textarea><div><button class="editbutton" id="editbtn-'
            + id + '" onclick="requestForumPostEdit(' + threadcode
            + "," + id + "," + 'getDatafromPostEditer(' + id + ')'
            + ');requestThread(' + threadcode + "," + pageencours
            + ')">Update post</button></div></div>';
            $('#Threadcontent').append(editdialogdiv);
            $('#editbtn-' + id).button();
            $("#postedittextara-" + id).markItUp(mySettings);

            $("#editdialogpost-" + id).dialog({
                autoOpen : false,
                show : "fade",
                hide : "fade",
                modal : true
            });

            (function() {
                var ids = id;
                var rawposts = rawpost;
                $("#postedit-" + id).click(function() {
                    showEditDialog(ids, rawposts);
                });
            })();
        }
    }

}

function getDatafromPostEditer(postid) {
    var text = $('textarea#postedittextara-' + postid).val();
    return text;
}

function showEditDialog(post, rawpost) {
    console.log("want to show dialog " + post + " " + rawpost);
    $("#editdialogpost-" + post).dialog("open");
}

function postText(threadid) {
    var text = $('textarea#markItUp').val();
    $('textarea#markItUp').val("");
    console.log('wanna post: ' + text + ' in thread: ' + threadid);
    var timeout = 30;
    $("#sendreplybutton").button("disable");
    var spamwaiter = setInterval(function() {
        $("#sendreplybutton").button("disable");
        $("#sendreplybutton").button("option", "label",
            'Wait: ' + --timeout + ' seconds');

        if (timeout == 0) {
            $("#sendreplybutton").button("option", "label", 'Send my reply');
            $("#sendreplybutton").button("enable");
            window.clearInterval(spamwaiter);
        }
    }, 1000);

    requestForumPostSend(threadid, text);
}

function putForumItem(forum) {

    var image = '<img src="' + getImageForum(forum[3]) + '">';

    var threadnameflag = '<table><tr><td>' + forum[2]
    + '</td></tr><tr><td><img src="' + getImageLang(forum[1])
    + '"></td></tr></table>';
    var wholethingy = '<div style="cursor: pointer;" onclick="requestForum('
    + forum[0] + ');"><table><tr><td align="left">' + image
    + '</td><td align="left">' + threadnameflag
    + '</td></tr></table></div>';

    var div = '<div class="ui-content">' + wholethingy + '</div>';

    $('#ForumsAndThreads').append(div);
}
function getLang() {
    return defaultlang;
}
function getImageLang(lang) {
    if (lang == "xx")
        return "img/flags/tfmint.png"

    for ( var i = 0; i < langs.length; i++) {
        if (langs[i].toLowerCase() == lang) {
            if (lang == "en") {
                return 'img/flags/gb.png';
            }
            return 'img/flags/' + lang + '.png';
        }

    }

    return "failure";

}
function getImageForum(iconcode) {
    if (iconcode == 1)
        return "img/Announcements.png";
    else if (iconcode == 2)
        return "img/Discussions.png";
    else if (iconcode == 3)
        return "img/OffTopic.png";
    else if (iconcode == 4)
        return "img/MapEditor.png";
    else if (iconcode == 5)
        return "img/Tribes.png";
    else if (iconcode == 6)
        return "img/FanArt.png";
    else if (iconcode == 7)
        return "img/suggestions.png";
    else if (iconcode == 8)
        return "img/Bugs.png";
    else if (iconcode == 9)
        return "img/tribeforum.png";
    else if (iconcode == 10)
        return "img/MapEditor.png";
    else if (iconcode == 11)
        return "img/OffTopic.png";
    return "";
}

function getColorDivCode(tigColorShortCut) {

    if (tigColorShortCut == "BV")
        return 1;
    else if (tigColorShortCut == "R")
        return 2;
    else if (tigColorShortCut == "BL")
        return 3;
    else if (tigColorShortCut == "J")
        return 4;
    else if (tigColorShortCut == "N")
        return 5;
    else if (tigColorShortCut == "G")
        return 6;
    else if (tigColorShortCut == "V")
        return 7;
    else if (tigColorShortCut == "VP")
        return 8;
    else if (tigColorShortCut == "VI")
        return 9;
    else if (tigColorShortCut == "ROSE")
        return 10;
    else if (tigColorShortCut == "CV")
        return 11;
    else if (tigColorShortCut == "CH")
        return 13;
    else if (tigColorShortCut == "CR")
        return 14;
    else if (tigColorShortCut == "T")
        return 15;
    else if (tigColorShortCut == "CJ")
        return 17;

    return 5;
}

function postTextCleaner(post) {
    // console.log(post);
    var postcleaned = post;
    postcleaned = postcleaned.replace(/amp;amp;/g, "");
    postcleaned = postcleaned.replace(/\n/g, "<br />");

    // parse color tags like <CV>
    var usedlangs = new Array();
    var imgregex = /\[\#(.{2})\](.*?)/;
    var temppost = postcleaned;
    while (temppost.match(imgregex)) {
        usedlangs.push(RegExp.$1);

        temppost = temppost
        .substring(temppost.indexOf('[#' + RegExp.$1 + ']') + 1);
    }


    if (usedlangs.length > 0) {
        var images = "";
        var langposts = "";
        // console.log(usedlangs);
        var startlang = currentLangPostId;
        var endlang = currentLangPostId + usedlangs.length;
        for (i = 0; i < usedlangs.length; i++) {
            images += '<img id="imgshower-' + currentLangPostId
            + '" hspace="5" src="'
            + getImageLang(usedlangs[i].toLowerCase())
            + '" onclick="flagClick(' + startlang + ',' + endlang + ','
            + currentLangPostId + ')">';
            langposts += parsePostLangSwitcher(usedlangs[i], postcleaned,
                i != 0)
            + "\n\n\n";
        }

        postcleaned = images + langposts;
    // console.log("post: "+images+"\n\n\n"+langposts);

    }
    var knowncolors = new Array("BV", "R", "BL", "J", "N", "G", "V", "VP",
        "VI", "ROSE", "CH", "T", "CR", "CV", "CJ");
    for ( var i = 0; i < knowncolors.length; i++) {
        postcleaned = parseImgTag(parseTigColors(knowncolors[i], postcleaned));
    }
    postcleaned = parseTransformiceLinks(parseQuoteTag(parseUrlTags(parseColorTag(postcleaned))));

    return postcleaned;
}

function parseUrlTags(postcleaned) {
    var quoteregex = /\[url=(.*?)\](.*?)\[\/url]/;
    while (postcleaned.match(quoteregex)) {
        var link = RegExp.$1
        var text = RegExp.$2;
        var beginning = postcleaned.indexOf("[url=" + link + "]")
        var end = postcleaned.substring(beginning, postcleaned.length).indexOf(
            "[/url]")
        + beginning;

        postcleaned = postcleaned.substring(0, beginning) + '<a href="' + link
        + '">' + text + '</a>'
        + postcleaned.substring(end + 6, postcleaned.length);
    }
    return postcleaned;
}

function parseImgTag(postcleaned) {

    var imgregex = /\[img\](.*?)\[\/img\]/;
    while (postcleaned.match(imgregex)) {
        var pic = RegExp.$1
        var beginning = postcleaned.indexOf("[img]")
        var end = postcleaned.indexOf("[/img]");
        postcleaned = postcleaned.substring(0, beginning) + '<div><a href="'
        + pic + '" rel="lightbox"><img class="imagesInPost"  src="'
        + pic + '"></a></div>'
        + postcleaned.substring(end + 6, postcleaned.length);
    }
    return postcleaned;
}
function parseQuoteTag(postcleaned)

{
    var quoteregex = /\[quote=(.*?)\](.*?)\[\/quote\]/;
    while (postcleaned.match(quoteregex)) {
        var author = RegExp.$1
        var text = RegExp.$2;
        // console.log(author+" "+text);

        var beginning = postcleaned.indexOf("[quote=" + author + "]")
        var end = postcleaned.substring(beginning, postcleaned.length).indexOf(
            "[/quote]")
        + beginning;
        postcleaned = postcleaned.substring(0, beginning)
        + '<div class="quote">' + '<div class="quoting">' + author
        + "</div>" + text + '</div>'
        + postcleaned.substring(end + 8, postcleaned.length);
    }

    quoteregex = /\[quote](.*?)\[\/quote\]/;
    while (postcleaned.match(quoteregex)) {
        text = RegExp.$1
        beginning = postcleaned.indexOf("[quote]")
        end = postcleaned.indexOf("[/quote]");
        postcleaned = postcleaned.substring(0, beginning)
        + '<div class="quote">' + '<div class="quoting">' + "</div>"
        + text + '</div>'
        + postcleaned.substring(end + 8, postcleaned.length);
    }

    return postcleaned;
}
function parseTransformiceLinks(postcleaned) {
    // console.log(postcleaned);
    var i = 0;
    // http://www.transformice.com/forum/?s

    var quoteregex = /http:\/\/www.transformice.com\/forum\/\?s=(\d*)&amp;p=(\d*)/;
    while (postcleaned.match(quoteregex)) {
        var topic = RegExp.$1;
        var page = RegExp.$2;
        postcleaned = postcleaned.replace(
            'http://www.transformice.com/forum/?s=' + topic + '&amp;p='
            + page, '<a href="' + baseurl + '?s=' + topic + '&p='
            + page + '">Topic: ' + topic + '</a>');
    }
    quoteregex = /http:\/\/www.transformice.com\/forum\/\?s=(\d*)&p=(\d*)/;
    while (postcleaned.match(quoteregex)) {
        topic = RegExp.$1;
        page = RegExp.$2;
        postcleaned = postcleaned.replace(
            'http://www.transformice.com/forum/?s=' + topic + '&p=' + page,
            '<a href="' + baseurl + '?s=' + topic + '&p=' + page
            + '">Topic: ' + topic + '</a>');
    }

    quoteregex = /http:\/\/www.transformice.com\/forum\/\?s=(\d*)/;
    while (postcleaned.match(quoteregex)) {
        topic = RegExp.$1;
        postcleaned = postcleaned.replace(
            'http://www.transformice.com/forum/?s=' + topic, '<a href="'
            + baseurl + '?s=' + topic + '&p=' + 0 + '">Topic: '
            + topic + '</a>');
    }

    return postcleaned;
}

function parseColorTag(postcleaned) {
    var colorregex = /\[color=(.*?)\](.*?)\[\/color\]/;
    while (postcleaned.match(colorregex)) {
        var color = RegExp.$1
        var text = RegExp.$2;

        var beginning = postcleaned.indexOf("[color=" + color + "]"+text+"[/color]")
        var end = beginning+8+color.length+text.length+8;
        postcleaned = postcleaned.substring(0, beginning)
        + '<span style="color:' + color + '">' + text + '</span>'
        + postcleaned.substring(end, postcleaned.length);

    }
    return postcleaned
}
function flagClick(startlang, endlang, activateflag) {
    for ( var i = startlang; i < endlang; i++) {
        $("#hider-" + i).hide("drop", {
            direction : "down"
        }, "fast");
    }

    $("#hider-" + activateflag).show("slow");
}

function parsePostLangSwitcher(lang, post, hide) {
    var endlang = post.indexOf('[/#' + lang.toLowerCase() + ']');
    if (endlang == -1) {
        return "";
    } else {
        var startlang = post.indexOf('[#' + lang.toLowerCase() + ']');
        return '<div id="hider-' + (currentLangPostId++) + '" '
        + (hide ? ('style="display: none"') : "") + '>' + "<N>"
        + post.substring(startlang + 5, endlang) + "</div>";
    }

    return post;
}
function parseTigColors(color, text) {
    var colortag = '<' + color + '>';
    var postcleaned = text;
    while (postcleaned.indexOf(colortag) != -1) {
        var colorpos = postcleaned.indexOf(colortag);
        postcleaned = postcleaned.substring(0, colorpos)
        + getColorSpan(postcleaned.substring(colorpos + 2
            + color.length, postcleaned.length),
        getColorDivCode(color));
    }
    return postcleaned;
}

function getColorSpanShort(name) {
    var Ausdruck = /<(\w.+)>(\w.+)/;
    Ausdruck.exec(name);
    if (RegExp.$1) {
        var type = RegExp.$1;

        return getColorSpan(RegExp.$2, getColorDivCode(type));
    }

    return getColorSpan(name, 19);

}
function getColorSpan(name, type) {
    return '<span class="colordiv-' + type + '">' + name + '</span>';
}

function populateGet() {
    var obj = {}, params = location.search.slice(1).split('&');
    for ( var i = 0, len = params.length; i < len; i++) {
        var keyVal = params[i].split('=');
        obj[decodeURIComponent(keyVal[0])] = decodeURIComponent(keyVal[1]);
    }
    return obj;
}
function loadDataOnUrl(timeout) {
    // console.log('baseurl: '+baseurl);

    var getsUrl = populateGet();
    var thread = getsUrl.s;
    var pagep = getsUrl.p;
    if (thread) {
        var page = 0;
        if (pagep)
            page = pagep;
        setTimeout('requestThread(' + thread + ', ' + (page - 1) + ')',
            timeout ? 500 : 0);

    }
}
