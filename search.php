<?php

// error_reporting(E_ALL);
// ini_set('display_errors', '1');
ini_set('default_charset', 'utf-8');

require_once 'settings.php';
if (isset($_POST["searchstring"]) && isset($_POST["searchoption"])) {
    $searchstring = $_POST["searchstring"];
    $searchoption = $_POST["searchoption"];
    $connect = mysqli_connect($DBhost, $DBusr, $DBpwd, $DBname);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    $searchstring = mysqli_real_escape_string($connect, $searchstring);
    $table_name = $DBposts;
    $sql;
    if ($searchoption === 'thread') {
        $table_name = $DBthread;
        $sql = "SELECT thread.threadid,thread.threadtitle,user.authorname FROM $table_name thread,$DBusers user WHERE thread.threadcreator = user.authorid
	AND thread.threadtitle LIKE '%$searchstring%' limit 0,30";
    } else {
        $sql = "SELECT thread.threadid,thread.threadtitle,user.authorname,post.postcontent,post.postid,post.ontfmpage FROM $table_name post,$DBthread thread,$DBusers user WHERE post.authorid=user.authorid AND post.threadid = thread.threadid AND  post.postcontent  LIKE '%$searchstring%'  limit 0,30";
    }

    $result = mysqli_query($connect, $sql);

    $rueckgabe = array();

    while ($row = mysqli_fetch_array($result)) {
        $searchresult = array('onpage' => $row['ontfmpage'], 'threadid' => $row['threadid'], 'threadtitle' => utf8_encode($row['threadtitle']), 'author' => $row['authorname'], 'post' => utf8_encode($row['postcontent']));
        $rueckgabe[] = $searchresult;
    }
    mysql_close();
    echo json_encode($rueckgabe);
}


